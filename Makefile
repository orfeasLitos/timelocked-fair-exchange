TEXFILES := $(shell find src/ -print)

timelocked-fair-exchange.pdf: $(TEXFILES)
	export TEXINPUTS=.:./src//:; \
	pdflatex --shell-escape -halt-on-error -interaction=nonstopmode timelocked-fair-exchange.tex; \
	rm -f timelocked-fair-exchange.{aux,log,out,toc,lof,lot,bbl,blg}

bib: $(TEXFILES)
	export TEXINPUTS=.:./src//:; \
	pdflatex --shell-escape -halt-on-error -interaction=nonstopmode timelocked-fair-exchange.tex; \
	bibtex timelocked-fair-exchange.aux; \
	pdflatex --shell-escape -halt-on-error -interaction=nonstopmode timelocked-fair-exchange.tex; \
	pdflatex --shell-escape -halt-on-error -interaction=nonstopmode timelocked-fair-exchange.tex; \
	rm -f timelocked-fair-exchange.{aux,log,out,toc,lof,lot,bbl,blg}

clean:
	rm -f *.aux *.log *.out *.toc *.lof *.lot *.bbl *.blg *.pdf
