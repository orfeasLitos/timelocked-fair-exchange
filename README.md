## Correctness & Security
A and B want to exchange secrets a and b. If, after committing to doing so within L blocks, E ∈ {A, B} doesn't, she is slashed and her secret is disclosed automatically anyway after k PoW. On the flip side, E can learn {A, B} \ {E}'s secret as well, but she has to burn at least k PoW locally on her own.

## References
https://dl.acm.org/doi/book/10.5555/888615
https://eprint.iacr.org/2021/715.pdf
https://eprint.iacr.org/2015/482.pdf
https://eprint.iacr.org/2015/574.pdf
https://eprint.iacr.org/2014/129.pdf
